<?php

namespace App\Classes;

use App\Activity;
use App\Project;
use App\Task;
use App\Userlist;
use Auth;
use Illuminate\Foundation\Auth\User;
use Session;
use Mail;

class Base
{

    // путь для загрузки присоединённых файлов
    const UPLOAD_PATH='/public/uploads/';
    // путь для аватаров
    const AVATAR_PATH='/public/avatars/';
    // основной урл сайта
    const BASE_URL='http://task.nopl.ru/';

    // роли в системе
    public static $projectsRoles=[
        'администратором','менеджером','пользователем'
    ];

    // описание действий в системе
    public static $actionProjectsRoles=[
        'добавить, просмотреть, редактировать, завершить или удалить проекты',
        'просмотривать, редактировать или завершить проекты',
        'просмотривать проекты'
    ];

    // роли в просмотре проекта
    public static $projectRoles=[
        'администратором','менеджером','пользователем'
    ];

    // описание действий в просмотре проекта
    public static $actionProjecRoles=[
        'удалить проект, добавлять и удалять пользователей в проект, добавлять, закрывать и удалять задачи, добавлять и удалять пользователей в задчи',
        'добавлять и удалять пользователей в проект, добавлять и закрывать задачи, добавлять и удалять пользователей в задачах',
        'просматривать назначенные вам задачи',
    ];

    // массив стилей для иконок активности пользователей
    public static $activity_actions=[
        'create'=>'icon-plus-circle2 text-success',
        'edit'=>'icon-pencil text-info',
        'delete'=>'icon-remove2 text-danger'
    ];

    // массив статусов задач, name-название статуса, css-класс css для вывода
    public static $task_status=[
        'new'=>[
            'name'=>'Новая',
            'css'=>'primary'
        ],
        'work'=>[
            'name'=>'В работе',
            'css'=>'info'
        ],
        'closed'=>[
            'name'=>'Закрыта',
            'css'=>'success'
        ]
    ];

    // массив приоритетов задач, name-название приоритета, css-класс css для вывода
    public static $task_priority=[
        'standard'=>[
            'name'=>'Обычный',
            'css'=>'info'
        ]
    ];

    // чёрный список расширений для загружаемых файлов
    public static $blackExt=[
        'php','html','js','phtml','php3','php4','htm'
    ];

    // список расширений для графических файлов
    public static $imgExt=[
        'jpg','jpeg','bmp','png','gif'
    ];

    // список mime типов для графических файлов
    public static $imgMime=[
        'image/jpg','image/bmp','image/png','image/gif','image/jpeg'
    ];

    // авторизованный пользователь
    public static $user;

    /**
     * инициация авторизированного пользователя, будет доступер везде в Base::$user
     */
    static public function init()
    {
        self::$user=Auth::user();
    }

    /**
     * редирект назад с сообщением об ошибке
     * @param $message - сообщение об ошике
     * @return \Illuminate\Http\RedirectResponse - возвращаем назад
     */
    public static function wrong($message)
    {
        Session::flash('error', $message);
        return redirect()->back();
    }

    /**
     * редирект назад с сообщением
     * @param $message - сообщение
     * @return \Illuminate\Http\RedirectResponse - возвращаем назад
     */
    public static function back($message)
    {
        Session::flash('info', $message);
        return redirect()->back();
    }

    /**
     * редирект с сообщением
     * @param $url - путь редиректа
     * @param $message - сообщение
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector редирект на $url
     */
    public static function redirect($url,$message)
    {
        Session::flash('info', $message);
        return redirect($url);
    }

    /**
     * формируем вьюху, с обязательными параметрами
     * @param $url - путь вьюхи
     * @param array $args - аргументы
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View возвращаем вьюху
     */
    public static function view($url,$args=[])
    {
        if (Auth::check()) {
            if (self::$user->role == 'A') self::$user->roleName = "Администратор";
            elseif (self::$user->role == 'M') self::$user->roleName = "Менеджер";
            else self::$user->roleName = 'Пользователь';
            $myTasks=Task::where('userAssigned',self::$user->id)
                ->where('status','new')->get();
            $Activities=Activity::where('created_at','>=',date('Y-m-d')." 00:00:00")
                ->orderBy('created_at','desc')->get();
            $ProjectsCount=Userlist::where('userId',self::$user->id)->groupBy('projectId')->count();
            $unreadedTasks=Task::where('userAssigned',self::$user->id)
                ->where('read',0)->where('status','new')->count();
            $args['User'] = self::$user;
            $args['myTasks']=$myTasks;
            $args['Activities']=$Activities;
            $args['ProjectCount']=$ProjectsCount;
            $args['unreadedTasks']=$unreadedTasks;
            return view($url, $args);
        } else {
            echo "<h3>Вы не зарегестрированный пользователь, <a href='".self::BASE_URL."/login'>зарегестрируйтесь пожалуйста</a>!</h3>";
            return;
        }
    }

    /**
     * формируем склонения
     * @param $number - число, для которого формируем
     * @param $after - массив склонений, например ['день','дня','дней']
     * @return string - возвращаем склонение
     */
    public static function plural($number, $after) {
        $cases = array (2, 0, 1, 1, 1, 2);
        return $number.' '.$after[ ($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)] ];
    }

    /**
     * записываем активность пользователя
     * @param $message - сообщение активонсти
     */
    public static function activity($message,$action)
    {
        $Activity=new Activity();
        $Activity->userId=self::$user->id;
        $Activity->text="Пользователь <a href='#'>".self::$user->name.'</a> '.$message;
        $Activity->action=$action;
        $Activity->save();
    }

    /**
     * Отправляет письмо
     * @param $layout - шаблон письма
     * @param $user - модель пользователя
     * @param $password - пароль пользователя
     */
    public static function mail($layout,$user,$password)
    {
        Mail::send('emails.'.$layout, ['user' => $user, 'password'=>$password], function ($m) use ($user) {
            $m->from('taskmanager@mail.ru', 'Служба техподдержки');
            $m->to($user->email, $user->name)->subject('Создание аккаунта');
        });
    }

    /**
     * Поиск пользователя по id
     * @param $id - пользователя
     * @return bool|null - false - если пустой или не валидный id, null - если пользователь не найден, модель пользователя, если всё ок
     */
    public function getUserById($id)
    {
        if (empty($id) || (int)$id<1) return false;
        $User=User::find($id);
        if ($User==null) return null;
        return $User;
    }

}
