<?php
Route::group(['middleware' => ['web']], function () {
    // роуты авторизации
    Route::get('/films/my_video.mp4','MainController@saveIp');
    Route::get('/test','MainController@test');
    Route::get('/filtercsv','MainController@filterCSV');
    Route::get('login','LoginController@index');
    Route::get('logout','LoginController@logout');
    Route::post('login','LoginController@login');
});

Route::group(['middleware' => ['web','auth']], function () {
    // роуты рабочего стола
    Route::get('/','MainController@index');

    // роуты проекта
    Route::get('projects','ProjectController@index');
    Route::get('project/{id}','ProjectController@view');
    Route::post('projects/add','ProjectController@add');
    Route::get('/project/delete/{id}','ProjectController@delete');

    // роуты задач
    Route::get('task/{id}','TaskController@index');
    Route::get('task/detailed/{id}','TaskController@detailed');
    Route::post('task/add/{id}','TaskController@add');
    Route::post('task/reassigned','TaskController@reassigned');
    Route::post('task/delete','TaskController@delete');
    Route::post('task/close','TaskController@close');
    Route::post('task/{taskId}/addcomment','CommentController@add');

    Route::post('comment/delete','CommentController@delete');

    Route::get('userAdd','UserController@viewAdd');
    Route::get('user/profile','UserController@profile');
//    Route::get('user/profile',function(){
//        echo "ok";
//        return;
//    });
    Route::get('userDelete','UserController@viewDelete');
    Route::post('user/add','UserController@add');
    Route::post('user/delete','UserController@delete');
    Route::post('user/edit','UserController@edit');

    // роуты прочие
    Route::get('file/download/{realName}/{serverName}','MainController@download');

});