<?php
namespace App\Http\Controllers;

use App\Classes\Base;
use App\Comment;
use App\Task;
use Auth;
use App\User;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct();
        $this->request=$request->all();
    }

    /**
     * добавление комментария
     * @param $taskId - id задачи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function add($taskId)
    {
        try {
            if ((int)$taskId < 1) return Base::wrong('Не правильный id!');
            $Task = Task::find($taskId);
            if ($Task == null) return Base::wrong('Нет такойзадачи!');
            $text = $this->request['comment'];
            if (empty($text)) return Base::wrong('Комментарий пустой!');
            $Comment = new Comment();
            $Comment->text = $text;
            $Comment->taskId = $taskId;
            $Comment->userId = Base::$user->id;
            $Comment->save();
            Base::activity("добавил комментарий к задаче <a href='/task/detailed/$Task->id'>$Task->name</a>",'create');
            return redirect()->back();
        } catch (\Exception $err){
            return response([$err->getMessage(),$err->getTrace()]);
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

    public function delete()
    {
        $id=(int)$this->request['commentId'];
        $taskId=(int)$this->request['taskId'];
        if ($id<1 || $taskId<1) return Base::wrong("Не правильный id!");
        $Comment=Comment::find($id);
        if ($Comment==null) return Base::wrong("Нет такого комментария!");
        $Task=Task::find($taskId);
        if ($Task==null) return Base::wrong("Нет такой задачи!");
        if (Base::$user->role!='A' || $Comment->userId!=Base::$user->id) return Base::wrong("У вас нет прав на удаления этого комментария!");
        $Comment->delete();
        Base::activity("удалил комментарий у задачи <a href='/task/detailed/$Task->id'>$Task->name</a>",'create');
        return Base::back("Комментарий успешно удалён!");
    }

}
