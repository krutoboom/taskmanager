<?php
namespace App\Http\Controllers;

use App\Classes\Base;
use App\Project;
use App\Task;
use App\Comment;
use App\Userlist;
use Auth;
use App\User;
use Illuminate\Http\Request;

class ProjectController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct();
        $this->request=$request->all();
    }

    /**
     * страница добавления проекта
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        try {
            $allUsers = User::all();
            return Base::view('project/index', [
                'allUsers' => $allUsers
            ]);
        } catch (\Exception $err){
            return Base::view('errors/error', ['debug'=>true,'errMessage'=>$err->getMessage()]);
        }
    }

    /**
     * добавление проетка
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function add()
    {
        try {
            $Project = new Project();
            $Project->name = $this->request['name'];
            $Project->description = $this->request['description'];
            $Project->status = 'new';
            $Project->userId=Base::$user->id;
            $Project->save();
            $Users = $this->request['user'];
            foreach ($Users as $i => $id) {
                $Userlist = new Userlist();
                $Userlist->userId = $id;
                $Userlist->role = $this->request['role'][$i];
                $Userlist->projectId = $Project->id;
                $Userlist->save();
            }
            Base::activity("добавил проект <a href='/project/$Project->id'>$Project->name</a>",'create');
            return Base::redirect("/","Проект $Project->name успешно добавлен");
        } catch (\Exception $err){
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

    /**
     * страница просмотра задачь проекта
     * @param $id - id проекта
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function view($id)
    {
        try {
            if ((int)$id < 1) return Base::wrong("Не правильный id!");
            $Project = Project::find($id);
            if ($Project==null) return Base::wrong("Нет такого проекта!");
            $Tasks = Task::where('projectId', $id)->get();
            $allUsers=User::all();
            if (Base::$user->role=='A') $userProjectRole=1;
            else {
                $userProjectRole=Userlist::where('projectId',$Project->id)->where('userId',Base::$user->id)->first();
                if ($userProjectRole==null) return Base::wrong("Вы не назначены этому проекту!");
                $userProjectRole=$userProjectRole->role;
            }
            return Base::view('project/view', [
                'Project' => $Project,
                'Tasks' => $Tasks,
                'allUsers'=>$allUsers,
                'userProjectRole'=>$userProjectRole
            ]);
        } catch (\Exception $err){
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

    /**
     * удаление проекта
     * @param $id - id проекта
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        try {
            if ((int)$id < 1) return Base::wrong('Не правильный id!');
            $Project = Project::find($id);
            if ($Project == null) return Base::wrong('Нет такого проекта!');
            if (Base::$user->role != 'A') return Base::wrong('Нет прав для удаления!');
            $name = $Project->name;
            $Tasks=Task::where('projectId',$id)->get();
            foreach($Tasks as $Task)
            {
                $attachments = unserialize($Task->attachment);
                if (!empty($attachments)) {
                    foreach ($attachments as $file) {
                        unlink(base_path().Base::UPLOAD_PATH.$file['serverName']);
                    }
                }
                Comment::where('taskId',$Task->id)->delete();
                Userlist::where('taskId',$Task->id)->delete();
            }
            $Tasks=Task::where('projectId',$id)->delete();
            Userlist::where('projectId',$Project->id)->delete();
            $name=$Project->name;
            $Project->delete();
            Base::activity("удалил проект $name",'delete');
            return Base::back("Проект $name успешно удалён");
        } catch (\Exception $err){
            return response([$err->getMessage(),$err->getTrace()    ]);
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

}
