<?php
namespace App\Http\Controllers;

use App\Classes\Base;
use App\Comment;
use App\Project;
use App\Task;
use App\Userlist;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Session;
use Storage;
use DB;

class TaskController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct();
        $this->request=$request->all();
    }

    /**
     * страница добавления задачи
     * @param $id - id проекта
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index($id)
    {
        try {
            if ((int)$id < 1) return Base::wrong('Не правильный id!');
            $Project = Project::find($id);
            if ($Project == null) return Base::wrong('Нет такого проекта!');
            $allUsers=DB::select("select * from users where id in(select userId from userlist where projectId=$Project->id)");
            return Base::view('task/add', [
                'allUsers'=>$allUsers,
                'Project' => $Project,
                'id' => $id,
            ]);
        } catch (\Exception $err){
            return response([$err->getMessage(),$err->getTrace()]);
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

    /**
     * добавление задачи
     * @param $id - id проекта
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function add($id)
    {
        try {
            $Project = Project::find($id);
            if ((int)$id < 1) return Base::wrong('Не правильный id!');
            if (empty($Project)) return Base::wrong('Нет такого проекта!');
            if (empty($this->request['name'])) return Base::wrong('Нет имени у задачи!');
            if (empty($this->request['description'])) return Base::wrong('Нет описания у задачи!');
            $files=[];
            if (!empty($this->request['file']))
            {
                foreach($this->request['file'] as $i=>$file)
                {
                    if (empty($file)) continue;
                    if (in_array($file->getClientOriginalExtension(),Base::$blackExt)) return Base::wrong('Не допустимый тип файла!');
                    $files[$i]['realName']=$file->getClientOriginalName();
                    $files[$i]['serverName']=str_random(10).rand(0,microtime(true)).str_random(5);
                    $file->move(base_path().Base::UPLOAD_PATH,$files[$i]['serverName']);
                    chmod(base_path().Base::UPLOAD_PATH.$files[$i]['serverName'],0777);
                }
            }
            $Task = new Task();
            $Task->name = $this->request['name'];
            $Task->description = $this->request['description'];
            $Task->progress = 0;
            $Task->status = 'new';
            $Task->priority = 'standard';
            $Task->projectId = $id;
            $Task->userCreate=Auth::user()->id;
            $Task->userAssigned=$this->request['userAssigned'];
            $Task->attachment=serialize($files);
            $Task->save();
            $Users = $this->request['user'];
            foreach ($Users as $i => $_id) {
                $Userlist = new Userlist();
                $Userlist->userId = $_id;
                $Userlist->role = 3;
                $Userlist->taskId = $Task->id;
                $Userlist->save();
            }
            Base::activity("добавил задчу <a href='/task/detailed/$Task->id'>$Task->name</a>",'create');
            return Base::redirect("/project/$id","Задача $Task->name успешно добавлена");
        } catch (\Exception $err){
//            return response([$err->getMessage(),$err->getTrace()]);
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

    /**
     * вывод детального вида задачи
     * @param $id - id задачи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function detailed($id)
    {
        try {
            if ((int)$id < 1) return Base::wrong('Не верный id!');
            $Task = Task::find($id);
            if ($Task == null) return Base::wrong('Нет такой задачи!');
            $Project = Project::find($Task->projectId);
            if ($Project == null) return Base::wrong('У этой задачи нет проекта!!!');
            $userList=Userlist::where('projectId',$Project->id)->where('userId',Base::$user->id)->first();
            if ($userList==null && $Project->userId!=Base::$user->id) return Base::wrong("Вы не добавлены в проект!");
            $projectRole=$userList->role;
            $userList=Userlist::where('taskId',$Task->id)->where('userId',Base::$user->id)->first();
            if ($userList==null && $Task->userCreate!=Base::$user->id
                && $Task->userAssigned!=Base::$user->id && $projectRole!=1 && $projectRole!=2)
                return Base::wrong("Вы не добавлены в задачу!");
            $UserCreate = User::find($Task->userCreate);
            $UserAssigned = User::find($Task->userAssigned);
            $userList = Userlist::where('taskId', $id)->get();
            $allUsers = User::all();
            $Users=[];
            foreach($allUsers as $u) $Users[$u->id]=$u;
            $Comments=Comment::where('taskId',$id)->get();
            $attachment=unserialize($Task->attachment);
            if (!$attachment) $attachment=[];
            if (!$Task->read && $Task->userAssigned==Base::$user->id){
                $Task->read=true;
                $Task->save();
                Base::activity("посмотрел задачу <a href='/task/detailed/$Task->id'>$Task->name</a>",'edit');
            }
            return Base::view('/task/detailed', [
                'Project' => $Project,
                'Task' => $Task,
                'UserCreate' => $UserCreate,
                'UserAssigned' => $UserAssigned,
                'userList' => $userList,
                'Users'=>$Users,
                'Comments'=>$Comments,
                'attachment'=>$attachment
            ]);
        } catch(\Exception $err){
//            return response([$err->getMessage()]);
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

    /**
     * переназначение задачи
     * $userId - id пользователя, которому назначаем
     * $taskId - id задачи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function reassigned()
    {
        try {
            $userId = !empty($this->request['reassignedUser']) ? $this->request['reassignedUser'] : 0;
            $taskId = !empty($this->request['taskId']) ? $this->request['taskId'] : 0;
            if ((int)$userId < 1 || (int)$taskId < 1) return Base::wrong('Не правильный id!');
            $Task = Task::find($taskId);
            if ($Task == null) return Base::wrong('Нет такой задачи!');
            $User = User::find($userId);
            if ($User == null) return Base::wrong('Нет такого пользователя!');
            $Task->userAssigned = $userId;
            $Task->save();
            Base::activity("нзаначил задачу <a href='/task/detailed/$Task->id'>$Task->name</a> пользователю <a href='#'>$User->name</a>",'edit');
            return Base::back("Задача $Task->name назначена пользователю $User->name");
        } catch(\Exception $err){
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

    /**
     * удаление задачи
     * $taskId - id задачи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function delete()
    {
        try {
            $taskId = !empty($this->request['taskId']) ? $this->request['taskId'] : 0;
            if ((int)$taskId < 1) return Base::wrong('Не правильный id!');
            $Task = Task::find($taskId);
            if ($Task == null) return Base::wrong('Нет такой задачи!');
            $attachments = unserialize($Task->attachment);
            if (!empty($attachments)) {
                foreach ($attachments as $file) {
                    unlink(base_path().Base::UPLOAD_PATH.$file['serverName']);
                }
            }
            Comment::where('taskId',$taskId)->delete();
            Userlist::where('taskId',$taskId)->delete();
            $name=$Task->name;
            $Task->delete();
            Base::activity("удалил задачу $name",'delete');
            return Base::back("Задача $name и связанные с ней файлы удалена");
        } catch(\Exception $err){
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

    /**
     * Закрытие задачи
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function close()
    {
        try {
            $taskId = !empty($this->request['taskId'])?$this->request['taskId']:0;
            if (empty($taskId)) return Base::wrong('Нет id!');
            if ((int)$taskId < 1) return Base::wrong('Не правиьный id!');
            $Task = Task::find($taskId);
            if ($Task == null) return Base::wrong('Нет такой задачи!');
            $Task->status = 'closed';
            $Task->save();
            return Base::back('Задача успешно закрыта!');
        } catch(\Exception $err){
            return response([$err->getMessage(),$err->getTrace()]);
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

}
