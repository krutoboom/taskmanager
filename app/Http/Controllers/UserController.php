<?php
namespace App\Http\Controllers;

use App\User;
use App\Userlist;
use Illuminate\Http\Request;
use App\Classes\Base;
use Response;
use Hash;
use Auth;
use App\Classes\SimpleImage;

class UserController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct();
        $this->request=$request->all();
    }

    /**
     * вывод страницы профиля
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile()
    {
        return Base::view('users.profile');
    }

    /**
     * Изменение данных пользователя
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit()
    {
        try {
            if (empty($this->request['name'])) return Base::wrong("Не заполнено имя пользователя!");
            if (empty($this->request['email'])) return Base::wrong("Не заполнен email!");
            if ($this->request['password'] != $this->request['confirm_password'])
                return Base::wrong("Не совпадают пароль и проверочный пароль!");
            $User = User::find(Base::$user->id);
            if ($User == null) {
                Auth::logout();
                return redirect('/');
            }
            if (!empty($this->request['avatar'])){
                $avatar=$this->request['avatar'];
                $ext=$avatar->getClientOriginalExtension();
                if (in_array($ext,Base::$blackExt)) return Base::wrong('Не допустимый тип файла!');
                if (!in_array(strtolower($avatar->getClientOriginalExtension()),Base::$imgExt) ||
                    !in_array($this->request['avatar']->getMimeType(),Base::$imgMime))
                        return Base::wrong('Аватар не является графическим файлом!');
                $fileName=str_random(10).rand(0,microtime(true)).str_random(5).'.'.$ext;
                $avatar->move(base_path().Base::AVATAR_PATH,$fileName);
                chmod(base_path().Base::AVATAR_PATH.$fileName,0777);
                if (empty($User->avatar)) $User->avatar=$fileName;
                else {
                    $unlink=@unlink(base_path().Base::AVATAR_PATH.$User->avatar);
                    $User->avatar=$fileName;
                }
                $image=new SimpleImage();
                $image->load(base_path().Base::AVATAR_PATH.$fileName);
                $image->resizeToWidth(300);
                $image->save(base_path().Base::AVATAR_PATH.$fileName);
            }
            $User->name = $this->request['name'];
            $User->email = $this->request['email'];
            if (!empty($this->request['password'])) $User->password = Hash::make($this->request['password'] . LoginController::SALT);
            $User->save();
            return Base::back("Данные успешно изменены!");
        } catch (\Exception $err){
//            return response([$err->getMessage(),$err->getTrace()]);
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

    /**
     * Страница добавления пользователя
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewAdd()
    {
        try {
            return Base::view('users.add');
        } catch (\Exception $err){
//            return response([$err->getMessage(),$err->getTrace()]);
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

    /**
     * Страница удаления пользователя
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewDelete()
    {
        try {
            $allUsers=User::all();
            return Base::view('users.delete',[
                'allUsers'=>$allUsers
            ]);
        } catch (\Exception $err){
//            return response([$err->getMessage(),$err->getTrace()]);
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

    /**
     * Добавление пользователя в систему
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|Response
     */
    public function add()
    {
        try {
            if (Base::$user->role != 'A') return Base::wrong('Вы не можете добавить пользователя!');
            if (empty($this->request['name']) || empty($this->request['email'])
                || empty($this->request['password']) || empty($this->request['role'])
            )
                return Base::wrong('Одно из полей не заполнено!');
            $User = new User();
            $User->name = $this->request['name'];
            $User->email = $this->request['email'];
            $User->role = $this->request['role'];
            $User->password = Hash::make($this->request['password'] . LoginController::SALT);
            $User->save();
            Base::mail('addUser',$User,$this->request['password']);
            return Base::back("Пользователь $User->name успешно добавлен!");
        } catch (\Exception $err){
            return response([$err->getMessage(),$err->getTrace()]);
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

    /**
     * Удаление пользователя из системы
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function delete()
    {
        try {
            if (Base::$user->role != 'A') return Base::wrong('Вы не можете удалить пользователя!');
            if ($this->request['user']==1 || $this->request['user']==2) return Base::wrong("Нельзя удалить этого пользователя, отсоси!!!");
            if (empty($this->request['user'])) return Base::wrong('Нет id пользователя!');
            if ((int)$this->request['user'] < 1) return Base::wrong("Нет такого пользователя!");
            $User = User::find($this->request['user']);
            if ($User == null) return Base::wrong("Нет такого пользователя!");
            $name = $User->name;
            Userlist::where('userId',$User->id)->delete();
            $User->delete();
            return Base::back("Пользователь $name успешно удалён");
        } catch (\Exception $err){
//            return response([$err->getMessage(),$err->getTrace()]);
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

}
