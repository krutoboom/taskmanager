<?php
namespace App\Http\Controllers;

use App\Project;
use App\Userlist;
use Illuminate\Http\Request;
use Auth;
use App\Classes\Base;
use Response;
use DB;

class MainController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct();
        $this->request=$request->all();
    }

    /**
     * рабочий стол, вывод всех доступных пользователю проектов
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        try {
            if (Base::$user->role=='A') $Projects = Project::all();
            else {
                $Projects=DB::select("select * from projects
                                      where userId=".Base::$user->id."
                                      or id in (
                                        select projectId from userlist
                                        where projectId is not null
                                        and userId=".Base::$user->id."
                                      )
                                      ");
            }
            if (Base::$user->role=='A') $userProjectRole=1;
            if (Base::$user->role=='M') $userProjectRole=2;
            if (Base::$user->role=='U') $userProjectRole=3;
            return Base::view('dashboard/index', [
                'Projects' => $Projects,
                'userProjectRole'=>$userProjectRole
            ]);
        } catch (\Exception $err){
            return response([$err->getMessage(),$err->getTrace()]);
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

    /**
     * загрузка файлов с сервера на клиент
     * @param $realName - имя файла
     * @param $serverName - серверное имя файла
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|Response
     */
    public function download($realName,$serverName)
    {
        try {
            return Response::download(base_path().Base::UPLOAD_PATH . $serverName,$realName);
        } catch (\Exception $err){
            return response([$err->getMessage(),$err->getTrace()]);
            return Base::view('errors/error', ['debug'=>false]);
        }
    }

    public function saveIp()
    {
        $file='getIp3.log';
        error_log("---Begin record---\n",3,$file);
        error_log('Date: '.date('Y-m-d H:m:s',time())."\n",3,$file);
        error_log('$_SERVER array:'."\n",3,$file);
//        foreach($_SERVER as $key=>$value){
//            error_log("\t".$key.": ".$value."\n",3,$file);
//        }
        $U=getenv("HTTP_USER_AGENT"); // получаем данные о софте,
        $H=getenv("HTTP_REFERER"); // получаем URL, с которого пришёл посетитель
        $R=getenv("REMOTE_ADDR"); // получаем IP посетителя
        $W=getenv("REQUEST_URI"); // получаем относительный адрес странички,
//        while (ereg('%([0-9A-F]{2})',$H)){ // пока в строке $H будет хоть одно
//            $val=ereg_replace('.*%([0-9A-F]{2}).*','\1',$H);
//            $newval=chr(hexdec($val)); // получаем сивол с номером,
//            $H=str_replace('%'.$val,$newval,$H);
//        }
        error_log('User agent:'.$U."\n",3,$file);
        error_log('Url referer:'.$H."\n",3,$file);
        error_log('IP:'.$R."\n",3,$file);
        error_log('Url origin:'.$W."\n",3,$file);
        error_log("---End record---\n\n",3,$file);
        echo "<h3>Если в течении 10 секунд вы не видите видео, <a href='#'>нажмите сюда</a> </h3>";
        return;
    }

    public function filterCSV()
    {
        \App::setLocale('ru');
        $fp = fopen('file1.csv', 'w');
        $date2=strtotime('2016-02-04 00:02:50');
        if (($handle = fopen("phones_all.csv", "r")) !== FALSE) {
//            $data=fgetcsv($handle, 0, ";");
//            $data=fgetcsv($handle, 0, ";");
//            echo iconv('windows-1251','utf-8',$data[11]);
//            dump($data);
//            if (strpos(iconv('windows-1251','utf-8',$data[11]),' Волгоградская обл.')!==false) echo 'ok';
            while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
                $date1=strtotime($data[1]);
//                if (!empty($data[3]) &&
//                    strpos(iconv('windows-1251','utf-8',$data[11]),'Волгоградская обл.')!==false &&
//                    strpos(iconv('windows-1251','utf-8',$data[11]),'Волжский г.')!==false)
                foreach($data as $d) {
                    if (strpos($d,'141.0.13.174')!==false)
                        fputcsv($fp, $data);
                }
            }
            fclose($handle);
        }
        fclose($fp);
        return "Done";
    }

    public function test()
    {
        if (empty($_REQUEST)) {
            $param = explode('?', $_SERVER['REQUEST_URI']);
            parse_str($param[1], $request);
        } else $request=$_REQUEST;
        $answer='unknown';
        if (isset($request['test'])){
            $n=$request['test'];
            if ($n=='1'){
                $answer=array(
                    "response"=>"вы оплатили",
                    'error'=>0
                );
            } else {
                $answer=array(
                    'status'=>'ok'
                );
            }
        }
        return json_encode($answer);
    }

}
