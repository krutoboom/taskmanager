@extends('layouts.default')
@section('title','Панель управления')
@section('content')
    @include('layouts.pageHeader',['pageHeader'=>'Ошибка 404'])
    <h2>Страница, которую вы запрашиваете, не существует! Убедитесь в правильности запроса!</h2>
@stop