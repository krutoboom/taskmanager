<li class="dropdown">
    <a data-toggle="dropdown" class="dropdown-toggle">
        <i class="icon-grid"></i>
        <span class="label label-danger" style="background-color: #D65C4F;">{{$unreadedTasks}}</span>
        <span class="label label-default">{{count($myTasks)}}</span>
    </a>
    <div class="popup dropdown-menu dropdown-menu-right">
        <div class="popup-header">
            <a href="#" class="pull-left"><i class="icon-spinner7"></i></a>
            <span>Мои задачи</span>
            <a href="#" class="pull-right"><i class="icon-new-tab"></i></a>
        </div>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Название</th>
                    <th class="text-center">Приоритет</th>
                </tr>
            </thead>
            <tbody>
            @foreach($myTasks as $Task)
                <tr>
                    <td>
                        <span class="status <?=$Task->read?'status-success':'status-danger'?> item-before"></span>
                        <a href="/task/detailed/{{$Task->id}}">{{$Task->name}}</a>
                    </td>
                    <td class="text-center"><span class="label label-success">{{$Task->progress}}%</span></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</li>