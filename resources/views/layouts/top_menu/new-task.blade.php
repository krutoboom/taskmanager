<li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown">
        <i class="icon-paragraph-justify2"></i>
        <span class="label label-default">0</span>
    </a>
    <div class="popup dropdown-menu dropdown-menu-right">
        <div class="popup-header">
            <a href="#" class="pull-left"><i class="icon-spinner7"></i></a>
            <span>Новые задачи</span>
            <a href="#" class="pull-right"><i class="icon-new-tab"></i></a>
        </div>
        <ul class="popup-messages">
            <li class="unread">
                <a href="#">
                    <img src="http://placehold.it/300" alt="" class="user-face">
                    <strong>Eugene Kopyov <i class="icon-attachment2"></i></strong>
                    <span>Aliquam interdum convallis massa...</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="http://placehold.it/300" alt="" class="user-face">
                    <strong>Jason Goldsmith <i class="icon-attachment2"></i></strong>
                    <span>Aliquam interdum convallis massa...</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="http://placehold.it/300" alt="" class="user-face">
                    <strong>Angel Novator</strong>
                    <span>Aliquam interdum convallis massa...</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="http://placehold.it/300" alt="" class="user-face">
                    <strong>Monica Bloomberg</strong>
                    <span>Aliquam interdum convallis massa...</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="http://placehold.it/300" alt="" class="user-face">
                    <strong>Patrick Winsleur</strong>
                    <span>Aliquam interdum convallis massa...</span>
                </a>
            </li>
        </ul>
    </div>
</li>