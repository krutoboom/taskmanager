<div class="sidebar">
    <div class="sidebar-content">

        <div class="user-menu dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?=!empty($User->avatar)?'/avatars/'.$User->avatar:'http://placehold.it/300'?>">
                <div class="user-info">
                    {{{$User->name}}} <span>{{{$User->roleName}}}</span>
                </div>
            </a>
            <div class="popup dropdown-menu dropdown-menu-right">
                <div class="thumbnail">
                    <div class="thumb">
                        <img src="<?=!empty($User->avatar)?'/avatars/'.$User->avatar:'http://placehold.it/300'?>">
                        <div class="thumb-options">
                            <span>
                                <a href="/user/profile" class="btn btn-icon btn-success"><i class="icon-pencil"></i></a>
                                <a href="#" class="btn btn-icon btn-success"><i class="icon-remove"></i></a>
                            </span>
                        </div>
                    </div>
                    <div class="caption text-center">
                        <h6>{{{$User->name}}} <small>{{{$User->roleName}}}</small></h6>
                    </div>
                </div>

                <ul class="list-group">
                    <li class="list-group-item"><i class="icon-pencil3 text-muted"></i> Проектов <span class="label label-success">{{$ProjectCount}}</span></li>
                    <li class="list-group-item"><i class="icon-people text-muted"></i> Задач <span class="label label-info">{{count($myTasks)}}</span></li>
                </ul>
            </div>
        </div>

        <ul class="navigation">
            <li class="<?=Request::segment(1)==''?'active':''?>">
                <a href="/"><span>Рабочий стол</span> <i class="icon-screen2"></i></a>
            </li>
            @if ($User->role=='A')
            <li class="<?=Request::segment(1)=='projects'?'active':''?>">
                <a href="/projects"><span>Проекты</span> <i class="icon-numbered-list"></i></a>
            </li>
            @endif
            <li class="<?=Request::segment(1)=='messages'?'active':''?>">
                <a href="/messages"><span>Сообщения</span> <i class="icon-bubbles5"></i></a>
            </li>
            @if ($User->role=='A')
                <li class="<?=Request::segment(1)=='users'?'active':''?>">
                    <a href="#"><span>Пользователи</span> <i class="icon-users"></i></a>
                    <ul>
                        <li class="<?=Request::segment(1)=='userAdd'?'active':''?>">
                            <a href="/userAdd"><span>Добавить</span></a>
                        </li>
                        <li class="<?=Request::segment(1)=='userDelete'?'active':''?>">
                            <a href="/userDelete"><span>Удалить</span></a>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>
</div>