<div class="page-header">
    <div class="page-title">
        <h3>{{$pageHeader}}
            @if (!empty($pageDescription))
            <small>{{$pageDescription}}</small>
            @endif
        </h3>
    </div>
</div>