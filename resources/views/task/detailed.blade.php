@extends('layouts.default')
@section('title','Панель управления')
@section('content')
    @include('layouts.pageHeader',['pageHeader'=>'Задача','pageDescription'=>'Детальный просмотр задачи'])
    @include('layouts.breadcrumbs',['urls'=>[
        ['uri'=>'/','name'=>'Рабочий стол'],
        ['uri'=>'/project/'.$Project->id,'name'=>$Project->name]],'currentUrl'=>$Task->name])
    @include('layouts.errors')

    <div class="row">
        <div class="col-lg-8">

            <!-- Task description -->
            <div class="block">
                <h5>{{$Task->name}}</h5>
                <ul class="headline-info">
                    <li>Проект: <a href="/project/{{$Project->id}}">{{$Project->name}}</a></li>
                    <li>Статус: <span class="text-semibold text-success">{{$Task->status}}</span></li>
                    <li>Создана: <a href="#">
                            @if(!empty($UserCreate)){{$UserCreate->name}}@endif
                        </a></li>
                    <li>Назначена:<a href="#">
                            @if(!empty($UserAssigned)){{$UserAssigned->name}}@else{{'Не назначена'}}@endif
                        </a></li>
                </ul>
                <hr>
                <div class="block-inner">
                    <p>{{$Task->description}}</p>
                </div>

                <div class="panel-footer">
                    <div class="pull-left">
                        <ul class="footer-links-group">
                            <li><i class="icon-plus-circle muted"></i> Добавлена: <a href="#" class="text-semibold">{{$Task->created_at}}</a></li>
                            <li><i class="icon-checkmark-circle muted"></i> Выполнить: <a href="#" class="text-semibold">{{!empty($Task->deadline)?$Task->deadline:'не назначено'}}</a></li>
                            <!--li class="has-label"><span class="label label-success">$Task->progress%</span></li-->
                            <li><i class="icon-bubble5 muted"></i> <a href="#" class="text-semibold">{{count($Comments)}}</a></li>
                        </ul>
                    </div>
                    <div class="pull-right">
                        <ul class="footer-icons-group">
                            <li><a href="#"><i class="icon-pencil"></i></a></li>
                            <li><a href="#"><i class="icon-checkmark3"></i></a></li>
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-wrench"></i></a>
                                <ul class="dropdown-menu icons-right dropdown-menu-right">
                                    <li><a href="#"><i class="icon-quill2"></i> Редактировать</a></li>
                                    <li><a href="#"><i class="icon-share2"></i> Переназначить</a></li>
                                    <li><a href="#"><i class="icon-checkmark3"></i> Выполнена</a></li>
                                    <li><a href="#"><i class="icon-stack"></i> В архив</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <h6 class="heading-hr"><i class="icon-bubble"></i> Комментарии</h6>
            <div class="block">
                @if (count($Comments)==0) <p>Нет комментариев!</p> @endif
                @foreach($Comments as $Comment)
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="<?=!empty($Users[$Comment->userId]->avatar)?'/avatars/'.$Users[$Comment->userId]->avatar:'http://placehold.it/300'?>" alt="">
                    </a>
                    <div class="media-body">
                        <a href="#" class="media-heading">{{$Users[$Comment->userId]->name}}</a>
                        <ul class="headline-info">
                            <!--li><a href="#">Редактировать</a></li-->
                            <li><a href="#deleteComment_modal" class="delete-comment" data-toggle="modal"
                                   data-id="{{$Comment->id}}" data-taskId="{{$Task->id}}">Удалить</a></li>
                        </ul>
                        {{$Comment->text}}
                    </div>
                </div>
                @endforeach
            </div>

            <div class="block">
                <h6><i class="icon-bubble-plus"></i> Добавить комментарий</h6>
                <div class="well">
                    <form action="/task/{{$Task->id}}/addcomment" method="post" role="form">

                        <div class="form-group">
                            <label>Комментарий:</label>
                            <textarea name="comment" rows="5" cols="5" placeholder="Сообщение..." class="elastic form-control"></textarea>
                        </div>

                        <div class="form-actions text-right">
                            <input type="submit" value="Добавить" class="btn btn-primary">
                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    </form>
                </div>
            </div>

        </div>

        <div class="col-lg-4">

            <div class="panel panel-success">
                <div class="panel-heading">
                    <h6 class="panel-title"><i class="icon-clock"></i> Таймер задачи</h6>
                </div>

                <div class="panel-body">
                    <ul class="timer-weekdays">
                        <li><a href="#" class="label label-default">Пн</a></li>
                        <li class="active"><a href="#" class="label label-danger">Вт</a></li>
                        <li><a href="#" class="label label-default">Ср</a></li>
                        <li><a href="#" class="label label-default">Чт</a></li>
                        <li><a href="#" class="label label-default">Пт</a></li>
                        <li><a href="#" class="label label-default">Сб</a></li>
                        <li><a href="#" class="label label-default">Вс</a></li>
                    </ul>

                    <ul class="timer">
                        <li>
                            00 <span>часов</span>
                        </li>
                        <li class="dots">:</li>
                        <li>
                            00 <span>минут</span>
                        </li>
                        <li class="dots">:</li>
                        <li>
                            00 <span>секунд</span>
                        </li>
                    </ul>
                </div>

                <div class="panel-footer">
                    <div class="pull-left">
                        <span><i class="icon-busy"></i> 0 часов</span>
                    </div>
                    <div class="pull-right">
                        <ul class="footer-icons-group">
                            <li><a href="#"><i class="icon-play2"></i></a></li>
                            <li><a href="#"><i class="icon-pause"></i></a></li>
                            <li><a href="#"><i class="icon-checkmark3"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h6 class="panel-title"><i class="icon-link"></i> Добавленные файлы</h6>
                </div>
                <ul class="list-group">
                    @if (empty($attachment)) <li class="list-group-item has-button">Нет прикреплённых файлов</li> @endif
                    @foreach($attachment as $file)
                        <li class="list-group-item has-button">
                            <i class="icon-file5"></i>
                            <a href="/file/download/{{$file['realName']}}/{{$file['serverName']}}">{{$file['realName']}}</a>
                            <a href="/file/download/{{$file['realName']}}/{{$file['serverName']}}" class="btn btn-link btn-icon">
                                <i class="icon-download"></i>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h6 class="panel-title"><i class="icon-people"></i> Добавленные пользователи</h6>
                </div>
                <ul class="message-list">
                    @foreach($userList as $user)
                    <li>
                        <div class="clearfix">
                            <div class="chat-member">
                                <a href="#"><img src="http://placehold.it/300" alt=""></a>
                                <h6>{{$Users[$user->userId]->name}}</h6>
                            </div>
                            <div class="chat-actions">
                                <a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-bubble3"></i></a>
                                <!--a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-phone2"></i></a-->
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div id="deleteComment_modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="icon-accessibility"></i> Удаление комментария</h4>
                </div>

                <div class="modal-body with-padding">
                    <p>Вы уверены, что хотите удалить комментарий?</p>
                </div>
                <form method="post" action="/comment/delete/">
                    <div class="modal-footer">
                        <button class="btn btn-warning" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-primary">Удалить</button>
                        <input type="hidden" name="commentId" value="0" id="deleteCommentInput">
                        <input type="hidden" name="taskId" value="0" id="deleteCommentTaskInput">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    </div>
                </form>

            </div>
        </div>
    </div>

<script>
    $(document).ready(function(){
        $('.delete-comment').click(function(){
            $('#deleteCommentInput').val($(this).data('id'));
            $('#deleteCommentTaskInput').val($(this).data('taskid'));
        });
    });
</script>

@stop