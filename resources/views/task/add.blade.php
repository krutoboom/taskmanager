@extends('layouts.default')
@section('title','Панель управления')
@section('content')
@include('layouts.pageHeader',['pageHeader'=>'Задача','pageDescription'=>'Добавление задачи проекту '.$Project->name])
@include('layouts.breadcrumbs',['urls'=>[
    ['uri'=>'/','name'=>'Рабочий стол'],
    ['uri'=>'/project/'.$Project->id,'name'=>$Project->name]],'currentUrl'=>'Задача'])
@include('layouts.errors')
    <form method="post" action="/task/add/{{$Project->id}}" enctype="multipart/form-data">
        <div class="form-group">
            <label>Название задачи</label>
            <input type="text" name="name" class="form-control">
        </div>
        <div class="form-group">
            <label>Описание задачи</label>
            <textarea name="description" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label>Прикрепить файлы</label>
            <input name="file[]" type="file" class="styled" multiple>
        </div>
        <div class="form-group row">
            <div class="col-md-4">
                <label>Назначить задачу:</label>
                <select name="userAssigned" class="form-control">
                    @foreach($allUsers as $user)
                        <option value="{{{$user->id}}}">{{{$user->name}}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <h6 class="heading-hr">Добавить пользователей в задачу</h6>
        <div class="form-group row">
            <div class="col-md-4">
                <label>Пользователь</label>
                <select name="user[]" class="form-control">
                    @foreach($allUsers as $user)
                        <option value="{{{$user->id}}}">{{{$user->name}}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <button id="add-user-btn" type="button" class="btn btn-success btn-icon"><i class="icon-plus"></i> </button>
        </div>
        <div class="form-group text-right">
            <input type="submit" class="btn btn-primary" value="Добавить">
        </div>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
    </form>
@stop