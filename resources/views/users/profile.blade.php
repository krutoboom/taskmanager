@extends('layouts.default')
@section('title','Панель управления')
@section('content')
    @include('layouts.pageHeader',['pageHeader'=>'Профиль','pageDescription'=>'Редактирование профиля'])
    @include('layouts.breadcrumbs',['urls'=>[
        ['uri'=>'/','name'=>'Рабочий стол'],],'currentUrl'=>'Профиль'])
    @include('layouts.errors')
    <form action="/user/edit" method="post" enctype="multipart/form-data">
        <div class="form-group row">
            <div class="col-md-6">
                <label>Имя пользователя:</label>
                <input name="name" type="text" class="form-control" value="{{$User->name}}">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label>Email:</label>
                <input name="email" type="text" class="form-control" value="{{$User->email}}">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label>Загрузить аватар:</label>
                <input name="avatar" type="file" class="styled">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label>Новый пароль:</label>
                <input name="password" type="password" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label>Подтвердите пароль:</label>
                <input name="confirm_password" type="password" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12 text-right">
                <button class="btn btn-primary" type="submit">Изменить данные</button>
            </div>
        </div>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
    </form>
@stop