@extends('layouts.default')
@section('title','Панель управления')
@section('content')
    @include('layouts.pageHeader',['pageHeader'=>'Добавление пользователя','pageDescription'=>'Добавление пользователя в систему'])
    @include('layouts.breadcrumbs',['urls'=>[
        ['uri'=>'/','name'=>'Рабочий стол'],],'currentUrl'=>'Добавление пользователя'])
    @include('layouts.errors')
    <form action="/user/add" method="post">
        <div class="form-group row">
            <div class="col-md-6">
                <label>Имя пользователя:</label>
                <input name="name" type="text" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label>Email:</label>
                <input name="email" type="text" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label>Пароль:</label>
                <input name="password" type="text" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label>Роль:</label>
                <select name="role" class="form-control">
                    <option value="A">Администратор</option>
                    <option value="M">Менеджер</option>
                    <option value="U">Пользователь</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12 text-right">
                <button class="btn btn-primary" type="submit">Добавить пользователя</button>
            </div>
        </div>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
    </form>
@stop