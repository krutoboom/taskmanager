@extends('layouts.default')
@section('title','Панель управления')
@section('content')
    @include('layouts.pageHeader',['pageHeader'=>'Удаление пользователя','pageDescription'=>'Удаление пользователя из системы'])
    @include('layouts.breadcrumbs',['urls'=>[
        ['uri'=>'/','name'=>'Рабочий стол'],],'currentUrl'=>'Удаление пользователя'])
    @include('layouts.errors')
    <form action="user/delete" method="post">
        <div class="form-group row">
            <div class="col-md-4">
                <label>Выберите пользователя:</label>
                <select id="selectUsers" name="user" class="form-control">
                    @foreach($allUsers as $user)
                        @if($user->id!=1 and $user->id!=2)
                            <option value="{{$user->id}}" data-role="{{$user->role}}" data-email="{{$user->email}}">{{$user->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="col-md-4">
                <label>Роль:</label>
                <input class="form-control" type="text" id="userRole" disabled value="<?php
                    if ($allUsers[2]->role=='A') echo "Администратор";
                        elseif ($allUsers[2]->role=='M') echo "Менеджер";
                            else echo "Пользователь";
                ?>">
            </div>
            <div class="col-md-4">
                <label>Email:</label>
                <input class="form-control" type="text" id="userEmail" disabled value="{{$allUsers[2]->email}}">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12 text-right">
                <button class="btn btn-danger" type="submit">Удалить пользователя</button>
            </div>
        </div>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
    </form>
<script>
    $(document).ready(function(){
        $('#selectUsers').change(function(){
            var role=$('#selectUsers option:selected').data('role');
            var roleName="";
            if (role=='A') roleName="Администратор";
            if (role=='M') roleName="Менеджер";
            if (role=='U') roleName="Пользователь";
            $('#userRole').val(roleName);
            $('#userEmail').val($('#selectUsers option:selected').data('email'));
        });
    });
</script>
@stop