@extends('layouts.default')
@section('title','Панель управления')
@section('content')
    <?php
    $projectRoleName=\App\Classes\Base::$projectsRoles[$userProjectRole-1];
    $actionRole=\App\Classes\Base::$actionProjectsRoles[$userProjectRole-1];
    ?>
@include('layouts.pageHeader',['pageHeader'=>'Рабочий стол','pageDescription'=>$User->name.' вы являетесь '.$projectRoleName.' в системе, вы можете '.$actionRole])
@include('layouts.breadcrumbs',['urls'=>[['uri'=>'/','name'=>'Рабочий стол']]])
@include('layouts.errors')
    @if (count($Projects)==0) <h3>У вас нет проектов</h3> @endif
    @foreach($Projects as $Project)
    <div class="col-md-6">
        <div class="block task task-high">
            <div class="row with-padding">
                <div class="col-sm-9">
                    <div class="task-description">
                        <a href="/project/{{{$Project->id}}}">{{{$Project->name}}}</a>
                        <span>{{{$Project->description}}}</span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="task-info">
                        <span>{{$Project->created_at}}</span>
                        <span><?php
                            $now=new \DateTime();
                            $old=new \DateTime($Project->created_at);
                            $diff=$now->diff($old);
                            echo \App\Classes\Base::plural($diff->days,['день','дня','дней']);
                            ?> | <span class="label label-danger">0%</span></span>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="pull-left">
                    <span><i class="icon-clock"></i></span>
                </div>
                <div class="pull-right">
                    <ul class="footer-icons-group">
                        @if ($User->role=='A' || $User->role=='M')
                            <li><a href="#"><i class="icon-pencil"></i></a></li>
                            @if ($User->role=='A')
                                <li><a  class="removeProject" data-toggle="modal" role="button" href="#removeProject_modal"
                                        data-id="{{$Project->id}}"><i class="icon-remove3"></i></a></li>
                            @endif
                            <li><a href="#"><i class="icon-checkmark3"></i></a></li>
                        @else <li><a href="#"><i class="icon-checkmark3 disabled"></i></a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endforeach
<div id="removeProject_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="icon-accessibility"></i> Удаление проекта</h4>
            </div>

            <div class="modal-body with-padding">
                <p>Вы уверены, что хотите удалить проект?</p>
            </div>

            <div class="modal-footer">
                <button class="btn btn-warning" data-dismiss="modal">Отмена</button>
                <button class="btn btn-primary deleteProject">Удалить</button>
            </div>
        </div>
    </div>
</div>
<script>
    var ProjectId=0;
    $(document).ready(function(){
        $('.removeProject').click(function(){
            ProjectId=$(this).data('id')
        });
        $('.deleteProject').click(function(){
            if (ProjectId==0) return;
            window.location.href="/project/delete/"+ProjectId;
        });
    });
</script>
@stop