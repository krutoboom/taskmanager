@extends('layouts.default')
@section('title','Панель управления')
@section('content')
@include('layouts.pageHeader',['pageHeader'=>'Проекты','pageDescription'=>'Добавление проектов'])
@include('layouts.breadcrumbs',['urls'=>[['uri'=>'/projects','name'=>'Проекты']]])
@include('layouts.errors')
    <form method="post" action="/projects/add">
        <div class="form-group row">
            <div class="col-md-6">
                <label>Название проекта:</label>
                <input name="name" type="text" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label>Описание проекта:</label>
                <textarea name="description" class="form-control"></textarea>
            </div>
        </div>
        <h6 class="heading-hr">Добавить пользоваетлей в проект</h6>
        <div id="assignedUsers">
            <div id="assignedUser_0" class="form-group row assigned">
                <div class="col-md-4">
                    <label>Пользователь</label>
                    <select name="user[]" class="form-control">
                        @foreach($allUsers as $user)
                            <option value="{{{$user->id}}}">{{{$user->name}}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Роль</label>
                    <select name="role[]" class="form-control">
                        <option value="1">Администратор</option>
                        <option value="2">Менеджер</option>
                        <option value="3">Пользователь</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <button data-id="0" type="button" class="add-user-btn btn btn-success btn-icon" style="margin-top: 21px">
                        <i class="icon-plus"></i>
                    </button>
                    <button data-id="0" type="button" class="delete-user-btn btn btn-danger btn-icon" style="margin-top: 21px;display: none">
                        <i class="icon-minus"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12 text-right">
                <button class="btn btn-primary" type="submit">Добавить проект</button>
            </div>
        </div>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
    </form>
<script>
    var countAssignedUsers=0;
    $(document).ready(function(){
        $('#assignedUsers').on('click','.add-user-btn',function(){
            countAssignedUsers=countAssignedUsers+1;
            var assignedContent=$('#assignedUser_0').clone();
            var addBtn=assignedContent.find('.add-user-btn');
            var deleteBtn=assignedContent.find('.delete-user-btn');
            addBtn.data('id',countAssignedUsers);
            deleteBtn.data('id',countAssignedUsers);
            assignedContent.attr('id','assignedUser_'+countAssignedUsers);
            deleteBtn.show();
            $('#assignedUsers').append(assignedContent);
        });
        $('#assignedUsers').on('click','.delete-user-btn',function(){
//            countAssignedUsers=countAssignedUsers-1;
            $('#assignedUser_'+$(this).data('id')).remove();
            $('.assigned').each(function(i,item){
                $(item).attr('id','assignedUser_'+i);
                var addBtn=$(item).find('.add-user-btn');
                var deleteBtn=$(item).find('.delete-user-btn');
                addBtn.data('id',i);
                deleteBtn.data('id',i);
            });
        });
    });
</script>
@stop