@extends('layouts.default')
@section('title','Панель управления')
@section('content')
    <?php
        $projectRoleName=\App\Classes\Base::$projectRoles[$userProjectRole-1];
        $actionRole=\App\Classes\Base::$actionProjecRoles[$userProjectRole-1];
    ?>
@include('layouts.pageHeader',['pageHeader'=>$Project->name,'pageDescription'=>$User->name.' вы являетесь '.$projectRoleName.' проекта, вы можете '.$actionRole])
@include('layouts.breadcrumbs',['urls'=>[['uri'=>'/','name'=>'Рабочий стол']],'currentUrl'=>$Project->name])
@include('layouts.errors')

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h6 class="panel-title">
                <a data-toggle="collapse" href="#descriptionProject" class="collapsed">Описание проекта {{$Project->name}}</a>
            </h6>
        </div>
        <div id="descriptionProject" class="panel-collapse collapse" style="height: 0px;">
            <div class="panel-body">
                {{$Project->description}}
            </div>
        </div>
    </div>

    <div class="tabbable page-tabs">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#all-tasks" data-toggle="tab"><i class="icon-paragraph-justify2"></i> Все задачи <span class="label label-info">{{count($Tasks)}}</span></a></li>
            <li><a href="#active" data-toggle="tab"><i class="icon-stack"></i> Активные</a></li>
            <li><a href="#closed" data-toggle="tab"><i class="icon-bubbles3"></i> Закрытые</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cogs"></i> Управление <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="/task/{{$Project->id}}"> Добавить задачу</a></li>
                    @if ($User->role=='A' or $Project->userId==$User->id or $userProjectRole==1 or $userProjectRole==2)
                        <li class="disabled"><a href="#"> Добавить пользователя</a></li>
                        <li class="disabled"><a href="#"> Удалить пользователя</a></li>
                    @endif
                </ul>
            </li>
        </ul>

        <div class="tab-content">

            <div class="tab-pane active fade in" id="all-tasks">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h6 class="panel-title"><i class="icon-paragraph-justify2"></i> Задачи</h6>
                        <span class="pull-right label label-danger">{{count($Tasks)}}</span>
                    </div>
                    <div class="datatable-tasks">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Описание задачи</th>
                                <th class="task-priority">Статус</th>
                                <th class="task-date-added">Дата добавления</th>
                                <th class="task-progress">Прогерсс</th>
                                <th class="task-deadline">Дедлайн</th>
                                <th class="task-tools text-center">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($Tasks as $Task)
                                <tr>
                                    <td class="task-desc">
                                        <a href="/task/detailed/{{$Task->id}}">{{$Task->name}}</a>
                                        <span>{{substr($Task->description,0,70).'...'}}</span>
                                    </td>
                                    <td class="text-center"><span class="label label-{{\App\Classes\Base::$task_status[$Task->status]['css']}}">{{\App\Classes\Base::$task_status[$Task->status]['name']}}</span></td>
                                    <td>{{$Task->created_at}}</td>
                                    <td>
                                        <div class="progress progress-micro">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{$Task->progress}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$Task->progress}}%;">
                                                <span class="sr-only">{{$Task->progress}}% выполнено</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <!--<i class="icon-clock"></i> <strong class="text-danger">14</strong> hours left-->
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-icon btn-success dropdown-toggle" data-toggle="dropdown"><i class="icon-cog4"></i></button>
                                            <ul class="dropdown-menu icons-right dropdown-menu-right">
                                                <li class="disabled">
                                                    <a href="#">
                                                        <i class="icon-quill2"></i> Редактировать
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#reassignedUser_modal" class="reassigned-task" data-toggle="modal" data-id="{{$Task->id}}">
                                                        <i class="icon-share2"></i> Переназначить
                                                    </a>
                                                </li>
                                                @if ($User->role=='A')
                                                    <li>
                                                        <a href="#deleteTask_modal" class="delete-task" data-toggle="modal" data-id="{{$Task->id}}">
                                                            <i class="icon-remove2"></i> Удалить
                                                        </a>
                                                    </li>
                                                @endif
                                                <li class="@if($Task->status=='closed') disabled @endif">
                                                    <a href="#closeTask_modal" class="close-task" data-toggle="modal" data-id="{{$Task->id}}">
                                                        <i class="icon-checkmark3"></i> Закрыть
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="active">
                <h3>Раздел "Активные" находится в разработке</h3>
            </div>
            <div class="tab-pane" id="closed">
                <h3>Раздел "Закрытые" находится в разработке</h3>
            </div>

        </div>
    </div>

<div id="reassignedUser_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="icon-paragraph-justify2"></i> Переназначить задачу</h4>
            </div>

            <form method="post" action="/task/reassigned/" role="form">
                <div class="modal-body with-padding">
                    <div class="block-inner text-info">
                        <h6 class="heading-hr">Выберите пользователя, которому хотите переназначить задачу</h6>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <select class="form-control" name="reassignedUser">
                                    @foreach($allUsers as  $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-primary">Назначить</button>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="taskId" id="reassignedUserInput" value="0">
            </form>
        </div>
    </div>
</div>
<div id="deleteTask_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="icon-accessibility"></i> Удаление задачи</h4>
            </div>

            <div class="modal-body with-padding">
                <p>Вы уверены, что хотите удалить задачу?</p>
            </div>
            <form method="post" action="/task/delete/">
                <div class="modal-footer">
                    <button class="btn btn-warning" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-primary">Удалить</button>
                    <input type="hidden" name="taskId" value="0" id="deleteTaskInput">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                </div>
            </form>

        </div>
    </div>
</div>
    <div id="closeTask_modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="icon-accessibility"></i> Закрытие задачи</h4>
                </div>

                <div class="modal-body with-padding">
                    <p>Вы уверены, что хотите закрыть задачу?</p>
                </div>
                <form method="post" action="/task/close/">
                    <div class="modal-footer">
                        <button class="btn btn-warning" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-primary">Закрыть</button>
                        <input type="hidden" name="taskId" value="0" id="closeTaskInput">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    </div>
                </form>

            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){

        $('.reassigned-task').click(function(){
            $('#reassignedUserInput').val($(this).data('id'));
        });

        $('.delete-task').click(function(){
            $('#deleteTaskInput').val($(this).data('id'));
        });

        $('.close-task').click(function(){
            $('#closeTaskInput').val($(this).data('id'));
        });
    });
</script>
@stop